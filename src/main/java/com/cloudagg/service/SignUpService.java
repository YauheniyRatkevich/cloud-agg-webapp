package com.cloudagg.service;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;

import com.cloudagg.converter.CloudUserConverter;
import com.cloudagg.dto.CloudUserDTO;
import com.cloudagg.entity.CloudUser;
import com.cloudagg.entity.UserRole;
import com.cloudagg.entity.enums.RoleEnum;
import com.cloudagg.repository.CloudUserRepository;
import com.cloudagg.repository.UserRoleRepository;
import com.cloudagg.service.exception.PasswordTooShortException;
import com.cloudagg.service.exception.UserAlreadyExistsException;

@Service
public class SignUpService {

	@Value("${credential.password.min-length}")
	private int MIN_PASSWORD_LENGTH;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private CloudUserRepository cloudUserRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private CloudUserConverter cloudUserConverter;

	public void persistUserAndSetSession(CloudUserDTO userDTO, HttpServletRequest request) {
		final String login = userDTO.getLogin();
		final String password = userDTO.getPassword();
		if (password.length() < MIN_PASSWORD_LENGTH) {
			//TODO ControllerAdvicer
			throw new PasswordTooShortException("The minimum length of password is " + MIN_PASSWORD_LENGTH);
		}
		final String hashPassword = passwordEncoder.encode(password);

		if (cloudUserRepository.findByLogin(login) != null) {
			//TODO ControllerAdvicer
			throw new UserAlreadyExistsException();
		}

		userDTO.setPassword(hashPassword);
		final CloudUser newUser = cloudUserRepository.save(cloudUserConverter.dtoToEntity(userDTO));
		newUser.setEnabled(true);
		userRoleRepository.save(new UserRole(newUser, RoleEnum.ROLE_USER));

		final UsernamePasswordAuthenticationToken userToken = new UsernamePasswordAuthenticationToken(login, hashPassword,
				Arrays.asList(new SimpleGrantedAuthority(RoleEnum.ROLE_USER.toString())));
		userToken.setDetails(new WebAuthenticationDetails(request));
		SecurityContextHolder.getContext().setAuthentication(userToken);
	}

}
