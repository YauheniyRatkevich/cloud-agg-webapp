'use strict';
define(["jquery"], function ($) {
    return {
        getToken: function (provider) {
            return $("meta[name='" + provider + "Token']").attr("content");
        }
    };
});