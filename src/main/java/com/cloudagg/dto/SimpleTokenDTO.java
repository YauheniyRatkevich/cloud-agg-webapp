package com.cloudagg.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpleTokenDTO implements Serializable {

	private static final long serialVersionUID = 6365292051748336001L;

	@JsonProperty("access_token")
	private String accessToken;

	@JsonProperty("token_type")
	private String tokenType;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}
