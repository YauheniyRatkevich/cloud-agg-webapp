package com.cloudagg.dto;

public class DropboxTokenDTO extends SimpleTokenDTO {

	private static final long serialVersionUID = -1928165259613290266L;

	private String uid;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
