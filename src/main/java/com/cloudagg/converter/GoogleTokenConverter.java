package com.cloudagg.converter;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.cloudagg.dto.GoogleTokenDTO;
import com.cloudagg.entity.GoogleAccessToken;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GoogleTokenConverter extends EntityConverter<GoogleTokenDTO, GoogleAccessToken> {

}
