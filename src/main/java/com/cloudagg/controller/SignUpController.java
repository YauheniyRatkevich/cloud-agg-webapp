package com.cloudagg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloudagg.dto.CloudUserDTO;
import com.cloudagg.service.SignUpService;

@Controller
public class SignUpController {

	@Autowired
	private SignUpService signUpService;

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String createNewUser(@Valid @ModelAttribute CloudUserDTO userDTO, HttpServletRequest request) {
		signUpService.persistUserAndSetSession(userDTO, request);
		return "redirect:/";
	}

}