package com.cloudagg.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cloudagg.service.LoginService;

@Controller
@RequestMapping("/")
public class ViewController {

	@Autowired
	private LoginService loginService;

	//FIXME remove/change
	//	@RequestMapping(value = "/{view}")
	//	public String viewResolver(@PathVariable("view") String view, Model model) {
	//		return view;
	//	}

	@RequestMapping
	public String index(Authentication auth, Model model) {
		if (auth == null) {
			return "forward:/index.html";
		}
		return "home";
	}

	@RequestMapping("/login")
	public String login(@RequestParam(name = "success", required = false) String success, Authentication auth, HttpSession session) {
		if (auth == null) {
			return "forward:/login-form.html";
		}
		loginService.loginSuccessHandler(auth, session);
		return "redirect:/";
	}
}
