package com.cloudagg.service.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudagg.configuration.bean.YandexOAuth2ClientConfiguration;
import com.cloudagg.converter.YandexTokenConverter;
import com.cloudagg.dto.YandexTokenDTO;
import com.cloudagg.repository.YandexAccessTokenRepository;

@Service
public class YandexOAuth2TokenService extends OAuth2TokenService<YandexTokenDTO> {

	@Autowired
	private YandexOAuth2ClientConfiguration clientConfiguration;

	@Autowired
	private YandexTokenConverter entityConverter;

	@Autowired
	private YandexAccessTokenRepository tokenRepository;

	@Override
	public YandexTokenConverter getEntityConverter() {
		return entityConverter;
	}

	@Override
	public YandexAccessTokenRepository getTokenRepository() {
		return tokenRepository;
	}

	@Override
	public YandexOAuth2ClientConfiguration getClientConfiguration() {
		return clientConfiguration;
	}

}