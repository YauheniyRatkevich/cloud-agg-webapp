package com.cloudagg.service;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.cloudagg.entity.CloudUser;
import com.cloudagg.repository.CloudUserRepository;
import com.cloudagg.repository.DropboxAccessTokenRepository;
import com.cloudagg.repository.GoogleAccessTokenRepository;
import com.cloudagg.repository.YandexAccessTokenRepository;
import com.cloudagg.service.oauth.OAuth2TokenService;

@Service
public class LoginService {

	@Autowired
	private CloudUserRepository cloudUserRepository;

	@Autowired
	private YandexAccessTokenRepository yandexTokenRepository;

	@Autowired
	private DropboxAccessTokenRepository dropboxTokenRepository;

	@Autowired
	private GoogleAccessTokenRepository googleTokenRepository;

	public void loginSuccessHandler(Authentication authentication, HttpSession session) {
		final CloudUser user = cloudUserRepository.findByLogin(authentication.getName());
		final Long userId = user.getUserId();

		OAuth2TokenService.setAccessTokenToSession(yandexTokenRepository.findOne(userId), session);
		OAuth2TokenService.setAccessTokenToSession(dropboxTokenRepository.findOne(userId), session);
		OAuth2TokenService.setAccessTokenToSession(googleTokenRepository.findOne(userId), session);
	}

}
