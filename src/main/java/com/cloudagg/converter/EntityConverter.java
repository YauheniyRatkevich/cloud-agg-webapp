package com.cloudagg.converter;

import java.io.Serializable;

public interface EntityConverter<DTO extends Serializable, Entity> {

	Entity dtoToEntity(DTO dto);

	DTO entityToDTO(Entity entity);
}
