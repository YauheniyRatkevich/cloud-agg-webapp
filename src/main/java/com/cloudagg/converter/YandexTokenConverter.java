package com.cloudagg.converter;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.cloudagg.dto.YandexTokenDTO;
import com.cloudagg.entity.YandexAccessToken;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface YandexTokenConverter extends EntityConverter<YandexTokenDTO, YandexAccessToken> {

}
