define([
        "jquery",
        "app/service/routes",
        "app/service/provider-api/yandex-api",
        "app/service/provider-api/dropbox-api",
        "app/service/provider-api/google-api",
        "PDFObject",
        "app/service/file/copy-move-file.service",
        "app/service/file/rename-file.service",
        "lodash",
        "app/service/provider-api/util/extract-link.service",

        "contextMenu"
    ], function ($, routes, yandexAPI, dropboxAPI, googleAPI, PDFObject, copyMoveFileService, renameFileService, _, extractLinkService) {
        function generateMenuItem(item, iconClass) {
            return '<i class="fa ' + iconClass + ' menu-icon"></i><span>' + item.name + '</span>';
        }

        var providersAggregator = {
            yandex: yandexAPI,
            dropbox: dropboxAPI,
            google: googleAPI
        };

        var menuClickItemHandler = {
            copyFile: function (filePath, item, providerName) {
                var fileName = item.context.attributes['title'].value;
                var filePath = item.context.attributes['context-info-path'].value;

                copyMoveFileService.openDialog(fileName, filePath, providerName);
            },
            renameFile: function (filePath, item, providerName) {
                var fileName = item.context.attributes['title'].value;
                var filePath = item.context.attributes['context-info-path'].value;

                renameFileService.openDialog(fileName, filePath, providerName);
            },
            deleteFile: function (filePath, item, providerName, methodKey, customMenuItems) {
                if (confirm("Are you sure, that you want to delete file?")) {
                    this.defaultHandler(filePath, item, providerName, methodKey, customMenuItems);
                }
            },
            defaultHandler: function (filePath, item, providerName, methodKey, customMenuItems) {
                providersAggregator[providerName][methodKey](filePath, function (response) {
                    customMenuItems[methodKey].success(item, response);
                });
            }
        };

        var items = {
            previewFile: {
                name: "Preview",
                icon: function (opt, $itemElement, itemKey, item) {
                    $itemElement.html(generateMenuItem(item, "fa-eye"));
                },
                success: function (item, response) {
                    var provider = item.context.attributes['context-info-provider'].value;
                    var link = document.createElement("a");
                    //TODO make another page in a new tab
                    if (provider === "dropbox") {
                        link.target = "_blank";
                        if (response.url) {
                            PDFObject.embed(response.url);
                            return;
                        }
                    }
                    link.href = extractLinkService.preview[provider](response);
                    link.click();
                },
                disabled: function () {
                    if ($(this).attr('context-info-provider') != "dropbox") {
                        return false;
                    } else {
                        var allowedExt = [".pdf", ".doc", ".docx", ".docm", ".ppt", ".pps", ".ppsx", ".ppsm", ".pptx", ".pptm",
                            ".xls", ".xlsx", ".xlsm", ".rtf"];
                        return _.indexOf(allowedExt, "." + $(this).attr('context-info-path').split('.').pop()) === -1;
                    }
                }
            },
            downloadFile: {
                name: "Download",
                icon: function (opt, $itemElement, itemKey, item) {
                    $itemElement.html(generateMenuItem(item, "fa-cloud-download"));
                },
                success: function (item, response) {
                    var provider = item.context.attributes['context-info-provider'].value;
                    var link = document.createElement("a");
                    link.download = "_"; //TODO retrieve filename
                    link.href = extractLinkService.download[provider](response);
                    link.click();
                },
                disabled: function () {
                    //TODO make folder download for dropbox and google
                    return $(this).attr('context-info-provider') != "yandex" && JSON.parse($(this).attr('context-info-is-dir'));
                }
            },
            copyFile: {
                name: "Copy",
                icon: function (opt, $itemElement, itemKey, item) {
                    $itemElement.html(generateMenuItem(item, "fa-files-o"));
                },
                success: function (item, response) {

                }
            },
            renameFile: {
                name: "Rename",
                icon: function (opt, $itemElement, itemKey, item) {
                    $itemElement.html(generateMenuItem(item, "fa-pencil-square"));
                },
                success: function () {
                    //TODO add handler
                }
            },
            // TODO: FEATURE WITH LOW PRIORITY
            // "shareFile": {
            //     name: "Share with...",
            //     icon: function (opt, $itemElement, itemKey, item) {
            //         $itemElement.html(generateMenuItem(item, "fa-share-alt"));
            //     },
            //     success: function () {
            //         //TODO: add handler
            //     }
            // },
            deleteFile: {
                name: "Delete",
                icon: function (opt, $itemElement, itemKey, item) {
                    $itemElement.html(generateMenuItem(item, "fa-trash"));
                },
                success: function (item) {
                    //TODO add confirmation
                    item.parent().remove();
                }
            }
        };

        $.contextMenu({
            selector: '.file-context',
            build: function ($trigger) {
                var customMenuItems = _.cloneDeep(items);

                if (JSON.parse($trigger.attr("context-info-is-dir"))) {
                    delete customMenuItems.previewFile;
                    delete customMenuItems.copyFile;
                }
                return {
                    callback: function (methodKey) {
                        var item = $(this);
                        var filePath = item.context.attributes['context-info-path'].value;
                        var providerName = item.context.attributes['context-info-provider'].value;

                        /**
                         * Choose provider-api by @providerName, methodKey to invoke by @methodKey with params: @filePath and success callback.
                         */
                        //TODO remade confirmation
                        if (menuClickItemHandler[methodKey]) {
                            menuClickItemHandler[methodKey](filePath, item, providerName, methodKey, customMenuItems);
                        } else {
                            menuClickItemHandler.defaultHandler(filePath, item, providerName, methodKey, customMenuItems);
                        }
                        console.log(methodKey + ": " + filePath);
                    },
                    items: customMenuItems
                };
            }
        });
    }
)
;