package com.cloudagg.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.PrimaryKeyJoinColumn;

@MappedSuperclass
public class AccessToken {

	@Id
	private Long userId;

	private String accessToken;

	private String tokenType;

	private LocalDateTime lastUpdated;

	@PrimaryKeyJoinColumn
	@OneToOne
	private CloudUser user;

	@PrePersist
	@PreUpdate
	public void prePersist() {
		lastUpdated = LocalDateTime.now();
	}
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public CloudUser getUser() {
		return user;
	}

	public void setUser(CloudUser user) {
		this.user = user;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.accessToken == null) ? 0 : this.accessToken.hashCode());
		result = prime * result + ((this.userId == null) ? 0 : this.userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AccessToken other = (AccessToken) obj;
		if (this.accessToken == null) {
			if (other.accessToken != null) {
				return false;
			}
		} else if (!this.accessToken.equals(other.accessToken)) {
			return false;
		}
		if (this.userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!this.userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("AccessToken [userId=%s, accessToken=%s, class=%s]", userId, accessToken, getClass());
	}
}
