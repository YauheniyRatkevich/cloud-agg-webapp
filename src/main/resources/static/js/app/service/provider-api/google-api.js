'use strict';
define([
    "jquery",
    "app/service/token.factory"
], function ($, tokenFactory) {

    function makeToken() {
        return "Bearer " + tokenFactory.getToken("google");
    }

    return {
        getFiles: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://www.googleapis.com/drive/v3/files?q='" + pathURI + "'+in+parents+and+trashed%3Dfalse&fields=files(appProperties%2Ccapabilities%2CcontentHints%2CcreatedTime%2Cdescription%2CexplicitlyTrashed%2CfileExtension%2CfolderColorRgb%2CfullFileExtension%2CheadRevisionId%2CiconLink%2Cid%2CisAppAuthorized%2Ckind%2ClastModifyingUser%2Cmd5Checksum%2CmimeType%2CmodifiedByMeTime%2CmodifiedTime%2Cname%2CoriginalFilename%2CownedByMe%2Cparents%2Cproperties%2CquotaBytesUsed%2Cshared%2CsharedWithMeTime%2CsharingUser%2Csize%2Cspaces%2Cstarred%2CthumbnailLink%2Ctrashed%2Cversion%2CviewedByMe%2CviewedByMeTime%2CviewersCanCopyContent%2CwebContentLink%2CwebViewLink%2CwritersCanShare)%2Ckind%2CnextPageToken",
                method: "GET",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("GOOGLE_GET_FILES_FAIL");
                //TODO handle failed requests
            });
        },
        getFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://www.googleapis.com/drive/v3/files/" + pathURI + "?fields=appProperties%2CcontentHints%2CcreatedTime%2Cdescription%2CexplicitlyTrashed%2CfileExtension%2CfolderColorRgb%2CfullFileExtension%2CheadRevisionId%2CiconLink%2Cid%2CisAppAuthorized%2Ckind%2ClastModifyingUser%2Cmd5Checksum%2CmimeType%2CmodifiedByMeTime%2CmodifiedTime%2Cname%2CoriginalFilename%2CownedByMe%2Cowners%2Cparents%2Cpermissions%2Cproperties%2CquotaBytesUsed%2Cshared%2CsharedWithMeTime%2CsharingUser%2Csize%2Cspaces%2Cstarred%2CthumbnailLink%2Ctrashed%2Cversion%2CvideoMediaMetadata%2CviewedByMe%2CviewedByMeTime%2CviewersCanCopyContent%2CwebContentLink%2CwebViewLink%2CwritersCanShare",
                method: "GET",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("GOOGLE_GET_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        deleteFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://www.googleapis.com/drive/v3/files/" + pathURI,
                method: "DELETE",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("GOOGLE_DELETE_FILE_FAIL");
                //TODO handle failed requests (especially 403 error)
            });
        },
        downloadFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://www.googleapis.com/drive/v2/files/" + pathURI +
                "?fields=description%2CdownloadUrl%2CexportLinks%2CfileExtension%2Cid%2CmimeType%2Ctitle",
                method: "GET",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (response.mimeType !== "application/vnd.google-apps.folder") {
                    if (callback) {
                        response.authorization = tokenFactory.getToken("google");
                        callback(response);
                    }
                }
                else {
                    alert("Google folder download has not implemented yet");
                }
            }).fail(function () {
                console.log("GOOGLE_DOWNLOAD_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        previewFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://www.googleapis.com/drive/v3/files/" + pathURI + "?fields=webViewLink",
                method: "GET",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("GOOGLE_PREVIEW_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        copyFile: function (fileId, dirDest, callback) {
            var pathURI = encodeURIComponent(fileId);
            var settings = {
                dataType: "json",
                contentType: "application/json",
                url: "https://www.googleapis.com/drive/v3/files/" + pathURI + "/copy",
                method: "POST",
                data: JSON.stringify({
                    parents: [
                        dirDest
                    ]
                }),
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("GOOGLE_COPY_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        saveUrl: function (fileName, dirDest, url, callback) {
            $.ajax({
                jsonpCallback: 'getFileId',
                contentType: "application/json",
                dataType: 'jsonp',
                url: "https://script.google.com/macros/s/AKfycbz47LO1rpqP9BIfoUCjD0FfcZktw4VWiirGqqN86GJImy_i4pFP/exec?name="
                + encodeURIComponent(fileName) + "&url=" + encodeURIComponent(url),
                method: "GET",
                success: function (response) {
                    var pathURI = encodeURIComponent(response.fileId);
                    var dirDestURI = encodeURIComponent(dirDest);
                    var settings = {
                        dataType: "json",
                        contentType: "application/json",
                        url: "https://www.googleapis.com/drive/v3/files/" + pathURI + '?addParents=' + dirDestURI + '&removeParents=root',
                        method: "PATCH",
                        headers: {
                            authorization: makeToken()
                        }
                    };

                    $.ajax(settings).done(function (response) {
                        if (callback) {
                            callback(response);
                        }
                    }).fail(function () {
                        //TODO handle failed requests
                        console.log("GOOGLE_SAVE_URL_FAIL");
                    });
                }
            });
        },
        renameFile: function (filePathSrc, fileNewName, callback) {
            var pathURI = encodeURIComponent(filePathSrc);
            var settings = {
                dataType: "json",
                contentType: "application/json",
                url: "https://www.googleapis.com/drive/v3/files/" + pathURI,
                method: "PATCH",
                data: JSON.stringify({
                    name: [
                        fileNewName
                    ]
                }),
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("GOOGLE_RENAME_FILE_FAIL");
                //TODO handle failed requests
            });
        }
    };
});