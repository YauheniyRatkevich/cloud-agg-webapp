package com.cloudagg.configuration.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "oauth.dropbox.client")
public class DropboxOAuth2ClientConfiguration extends OAuth2ClientConfiguration {

	private String redirectUri;

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}

}
