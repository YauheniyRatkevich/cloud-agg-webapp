'use strict';
define([
    "app/service/token.factory",
    "app/service/provider-api/yandex-api",
    "app/service/provider-api/dropbox-api",
    "app/service/provider-api/google-api",
    "lodash",

    "routie"
], function (tokenFactory, yandexAPI, dropboxAPI, googleAPI, _) {


    function shouldReloadFolders(provider, paths, scope) {
        return tokenFactory.getToken(provider) && (paths.reloadAll || paths.reload === provider
            || scope.prevPaths[provider] !== paths[provider]);
    }

    return {
        getOriginalProvidersPath: function () {
            return JSON.parse(decodeURIComponent(window.location.hash.split('/').pop()));
        },
        refreshContent: function (provider) {
            var originalPath = this.getOriginalProvidersPath();
            var newPath = _.clone(originalPath);
            if (provider) {
                newPath.reload = provider;
            } else {
                newPath.reloadAll = true;
            }
            routie('providers/' + encodeURIComponent(JSON.stringify(newPath)));
        },
        changePath: function (newPath) {
            var originalPath;
            if (window.location.hash) {
                originalPath = this.getOriginalProvidersPath();
            } else {
                originalPath = {};
            }
            var yandexPath;
            var dropboxPath;
            var googlePath;
            if (newPath) {
                yandexPath = newPath.yandex || originalPath.yandex || "/";
                dropboxPath = newPath.dropbox || originalPath.dropbox || "/";
                googlePath = newPath.google || originalPath.google || "root";
            } else {
                yandexPath = originalPath.yandex || "/";
                dropboxPath = originalPath.dropbox || "/";
                googlePath = originalPath.google || "root";
            }

            routie('providers/' + encodeURIComponent(JSON.stringify(
                    {
                        yandex: yandexPath,
                        dropbox: dropboxPath,
                        google: googlePath
                    }
                )));
        },
        registerRoutes: function (scope) {
            routie('providers/:providers', function (pathsParam) {
                if (!scope.files) {
                    scope.files = {
                        yandex: [],
                        dropbox: [],
                        google: []
                    };
                }

                if (!scope.prevPaths) {
                    scope.prevPaths = {};
                }

                var paths = JSON.parse(pathsParam);
                var fileIcon = 'img/file-ico.png';
                var dirIcon = 'img/folder-ico.png';
                var dirPrevIcon = 'img/folder-prev-ico.png';

                function addLinkToParentFolder(files, path) {
                    files.unshift({
                        isDir: true,
                        preview: dirPrevIcon,
                        name: '..',
                        path: path,
                        isPrevLink: true
                    });
                }

                function getUrlWithoutLastPart(url) {
                    return url.substr(0, url.lastIndexOf("/")) || "/";
                }

                if (shouldReloadFolders('yandex', paths, scope)) {
                    scope.showLoader.yandex = true;
                    scope.$scan();

                    yandexAPI.getFiles(paths.yandex, function (response) {
                        scope.files.yandex = _.map(response._embedded.items, function (file) {
                            file.isDir = (file.type === 'dir');
                            file.preview = file.preview || (file.isDir ? dirIcon : fileIcon);
                            return file;
                        });

                        if (paths.yandex !== "/" && paths.yandex !== "disk:") {
                            addLinkToParentFolder(scope.files.yandex, getUrlWithoutLastPart(paths.yandex));
                        }
                        scope.showLoader.yandex = false;
                        scope.$scan();
                    });
                }
                if (shouldReloadFolders('dropbox', paths, scope)) {
                    scope.showLoader.dropbox = true;
                    scope.$scan();

                    dropboxAPI.getFiles(paths.dropbox, function (response) {
                        scope.files.dropbox = _.map(response.contents, function (file) {
                            file.isDir = file.is_dir;
                            file.preview = file.isDir ? dirIcon : fileIcon;
                            file.name = file.path.split("/").pop();
                            return file;
                        });

                        if (paths.dropbox !== "/") {
                            addLinkToParentFolder(scope.files.dropbox, getUrlWithoutLastPart(paths.dropbox));
                        }
                        scope.showLoader.dropbox = false;
                        scope.$scan();
                    });
                }
                if (shouldReloadFolders('google', paths, scope)) {
                    scope.showLoader.google = true;
                    scope.$scan();

                    if (paths.google === "/") {
                        paths.google = "root";
                    }
                    googleAPI.getFiles(paths.google, function (response) {
                        scope.files.google = _.map(response.files, function (file) {
                            file.isDir = file.mimeType === "application/vnd.google-apps.folder";
                            file.preview = file.isDir ? dirIcon : (file.thumbnailLink || fileIcon);
                            file.path = file.id;
                            return file;
                        });

                        if (!scope.providerInfo.google.rootDirId) {
                            googleAPI.getFile("root", function (response) {
                                scope.providerInfo.google.rootDirId = response.id;
                                retrieveLinkToPrevDir();
                            });
                        } else {
                            retrieveLinkToPrevDir();
                        }

                        function retrieveLinkToPrevDir() {
                            scope.showLoader.google = false;

                            if (paths.google !== "/" && paths.google !== "root" && paths.google !== scope.providerInfo.google.rootDirId) {
                                googleAPI.getFile(paths.google, function (response) {
                                    addLinkToParentFolder(scope.files.google, response.parents[0]);
                                    scope.$scan();
                                });
                            } else {
                                scope.$scan();
                            }
                        }
                    });
                }
                scope.prevPaths = paths;

                if (paths.reload) {
                    delete paths.reload;
                    routie('providers/' + encodeURIComponent(JSON.stringify(paths)));
                }
            });
        }
    }
});