package com.cloudagg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloudagg.entity.DropboxAccessToken;

public interface DropboxAccessTokenRepository extends JpaRepository<DropboxAccessToken, Long> {

}
