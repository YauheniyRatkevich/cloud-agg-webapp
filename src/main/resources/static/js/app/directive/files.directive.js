'use strict';
define([
    "jquery",
    "alight",
    "app/service/token.factory",
    "lodash",
    "app/service/routes",
    
    "routie"
], function ($, alight, tokenFactory, _, routes) {
    alight.directives.al.files = {
        templateUrl: "js/app/template/files.template.html",
        link: function (scope) {
            scope.forward = function (file) {
                if (file.isDir) {
                    var path = file.path;
                    if (scope.provider.name === 'yandex' && tokenFactory.getToken("yandex")) {
                        routes.changePath({yandex: path});
                    } else if (scope.provider.name === 'dropbox' && tokenFactory.getToken("dropbox")) {
                        routes.changePath({dropbox: path});
                    } else if (scope.provider.name === 'google' && tokenFactory.getToken("google")) {
                        routes.changePath({google: path});
                    }
                } else {
                    //TODO handle click on file
                }
            };
        }
    };
});