package com.cloudagg.converter;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.cloudagg.dto.CloudUserDTO;
import com.cloudagg.entity.CloudUser;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CloudUserConverter extends EntityConverter<CloudUserDTO, CloudUser> {


}
