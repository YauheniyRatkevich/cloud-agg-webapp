package com.cloudagg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@EntityScan(basePackageClasses = { CloudaggWebApplication.class, Jsr310JpaConverters.class })
@SpringBootApplication
@EnableConfigurationProperties
public class CloudaggWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudaggWebApplication.class, args);
	}
}