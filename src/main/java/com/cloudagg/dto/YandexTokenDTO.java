package com.cloudagg.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class YandexTokenDTO extends SimpleTokenDTO {

	private static final long serialVersionUID = 998187846318127489L;

	@JsonProperty("expires_in")
	private Long expiresIn;

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}

}