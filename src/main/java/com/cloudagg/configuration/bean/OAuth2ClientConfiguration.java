package com.cloudagg.configuration.bean;


public abstract class OAuth2ClientConfiguration {

	private String id;

	private String secret;

	private String tokenUrl;

	private String authUrl;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(final String secret) {
		this.secret = secret;
	}

	public String getTokenUrl() {
		return tokenUrl;
	}

	public void setTokenUrl(final String tokenUrl) {
		this.tokenUrl = tokenUrl;
	}

	public String getAuthUrl() {
		return authUrl;
	}

	public void setAuthUrl(final String authUrl) {
		this.authUrl = authUrl;
	}
}
