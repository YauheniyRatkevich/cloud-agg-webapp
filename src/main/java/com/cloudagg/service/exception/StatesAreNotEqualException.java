package com.cloudagg.service.exception;


public class StatesAreNotEqualException extends RuntimeException {

	private static final long serialVersionUID = 4724030367447124305L;

	public StatesAreNotEqualException() {
	}

	public StatesAreNotEqualException(String message) {
		super(message);
	}

	public StatesAreNotEqualException(Throwable cause) {
		super(cause);
	}

	public StatesAreNotEqualException(String message, Throwable cause) {
		super(message, cause);
	}

	public StatesAreNotEqualException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
