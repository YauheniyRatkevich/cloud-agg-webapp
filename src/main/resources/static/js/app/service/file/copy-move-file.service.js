'use strict';
define([
    "jquery",
    "app/service/provider-api/yandex-api",
    "app/service/provider-api/dropbox-api",
    "app/service/provider-api/google-api",
    "app/service/provider-api/util/extract-link.service",
    "app/service/routes"
], function ($, yandexAPI, dropboxAPI, googleAPI, extractLinkService, routes) {
    var fileNameSrc;
    var filePathSrc;
    var providerNameSrc;
    var providerNameDest;

    var providersAggregator = {
        yandex: yandexAPI,
        dropbox: dropboxAPI,
        google: googleAPI
    };

    return {
        openDialog: function (fileName, filePath, provider) {
            fileNameSrc = fileName;
            filePathSrc = filePath;

            providerNameSrc = provider;
            $('#copyMoveDialog').modal();
        },
        closeDialog: function (scope, provider) {
            providerNameDest = provider;
            scope.copyDialogFileName = fileNameSrc;
            $("#copyMoveDialog").modal('hide');
        },
        copyFileSubmit: function (scope) {
            if (!window.location.hash) {
                //TODO alert
                alert("Wrong URL!");
                return;
            }

            scope.showLoader[providerNameDest] = true;
            scope.$scan();

            var originalProvidersPath = routes.getOriginalProvidersPath();
            var dirDest = originalProvidersPath[providerNameDest];

            if (providerNameSrc === providerNameDest) {
                providersAggregator[providerNameDest].copyFile(filePathSrc, dirDest, function () {
                    routes.refreshContent(providerNameDest);
                });
            } else {
                providersAggregator[providerNameSrc].downloadFile(filePathSrc, function (response) {
                    var downloadURL = extractLinkService.download[providerNameSrc](response);

                    providersAggregator[providerNameDest].saveUrl(fileNameSrc, dirDest, downloadURL, function () {
                        routes.refreshContent(providerNameDest);
                    });
                });
            }
        }
    }
});