package com.cloudagg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloudagg.entity.YandexUser;

public interface YandexUserRepository extends JpaRepository<YandexUser, Long> {

}
