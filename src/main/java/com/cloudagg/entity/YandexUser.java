package com.cloudagg.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class YandexUser {

	@Id
	private Long userId;

	@Column(nullable = false)
	private String yandexLogin;

	@Column(nullable = false)
	private String yandexId;

	private String defaultAvatarId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getYandexId() {
		return yandexId;
	}

	public void setYandexId(String yandexId) {
		this.yandexId = yandexId;
	}

	public String getDefaultAvatarId() {
		return defaultAvatarId;
	}

	public void setDefaultAvatarId(String defaultAvatarId) {
		this.defaultAvatarId = defaultAvatarId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((yandexId == null) ? 0 : yandexId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		YandexUser other = (YandexUser) obj;
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		if (yandexId == null) {
			if (other.yandexId != null) {
				return false;
			}
		} else if (!yandexId.equals(other.yandexId)) {
			return false;
		}
		return true;
	}

	public String getYandexLogin() {
		return yandexLogin;
	}

	public void setYandexLogin(String yandexLogin) {
		this.yandexLogin = yandexLogin;
	}

}
