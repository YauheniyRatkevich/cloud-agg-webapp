package com.cloudagg.configuration.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "oauth.yandex.client")
public class YandexOAuth2ClientConfiguration extends OAuth2ClientConfiguration {

}
