package com.cloudagg.service.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.cloudagg.configuration.bean.GoogleOAuth2ClientConfiguration;
import com.cloudagg.converter.GoogleTokenConverter;
import com.cloudagg.dto.GoogleTokenDTO;
import com.cloudagg.repository.GoogleAccessTokenRepository;

@Service
public class GoogleOAuth2TokenService extends OAuth2TokenService<GoogleTokenDTO> {

	@Autowired
	private GoogleOAuth2ClientConfiguration clientConfiguration;

	@Autowired
	private GoogleTokenConverter entityConverter;

	@Autowired
	private GoogleAccessTokenRepository tokenRepository;

	@Override
	public GoogleTokenConverter getEntityConverter() {
		return entityConverter;
	}

	@Override
	public GoogleAccessTokenRepository getTokenRepository() {
		return tokenRepository;
	}

	@Override
	protected void adjustRequestTokenForm(MultiValueMap<String, String> form) {
		form.add("redirect_uri", clientConfiguration.getRedirectUri());
	}

	@Override
	public GoogleOAuth2ClientConfiguration getClientConfiguration() {
		return clientConfiguration;
	}

}
