INSERT INTO cloud_users(login,password,enabled)
VALUES ('evgen','$2a$10$R24HmhCvf6NZzcBhZKtAr.j8FHC.CPCEx6S8bEPrQIi0fpQAoHw0G', true);
INSERT INTO cloud_users(login,password,enabled)
VALUES ('alex','$2a$10$R24HmhCvf6NZzcBhZKtAr.j8FHC.CPCEx6S8bEPrQIi0fpQAoHw0G', true);

INSERT INTO user_roles (user_id, role)
VALUES (1, 'ROLE_USER');
INSERT INTO user_roles (user_id, role)
VALUES (1, 'ROLE_ADMIN');
INSERT INTO user_roles (user_id, role)
VALUES (2, 'ROLE_USER');

INSERT INTO YANDEX_ACCESS_TOKEN  (USER_ID, ACCESS_TOKEN) 
VALUES (1, '2f7787fb2a864a2387dce1fb13ea53d4');
INSERT INTO DROPBOX_ACCESS_TOKEN  (USER_ID, ACCESS_TOKEN) 
VALUES (1, '4644DKHeFjAAAAAAAAABTie9Kgbr_LxDS1BtIAoHIerXAhXI-NlyzB8Ev6mtcVZl');