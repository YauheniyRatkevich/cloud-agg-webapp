package com.cloudagg.controller;

import java.text.MessageFormat;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cloudagg.configuration.bean.DropboxOAuth2ClientConfiguration;
import com.cloudagg.configuration.bean.GoogleOAuth2ClientConfiguration;
import com.cloudagg.configuration.bean.YandexOAuth2ClientConfiguration;
import com.cloudagg.service.oauth.DropboxOAuth2TokenService;
import com.cloudagg.service.oauth.GoogleOAuth2TokenService;
import com.cloudagg.service.oauth.YandexOAuth2TokenService;

@Controller
@RequestMapping("/oauth")
public class OAuth2Controller {

	private static final String CSRF_YANDEX = "csrf_yandex";

	private static final String CSRF_DROPBOX = "csrf_dropbox";

	private static final String CSRF_GOOGLE = "csrf_google";

	@Autowired
	private YandexOAuth2TokenService yandexTokenService;

	@Autowired
	private DropboxOAuth2TokenService dropboxTokenService;

	@Autowired
	private GoogleOAuth2TokenService googleTokenService;

	@Autowired
	private YandexOAuth2ClientConfiguration yandexClient;

	@Autowired
	private DropboxOAuth2ClientConfiguration dropboxClient;

	@Autowired
	private GoogleOAuth2ClientConfiguration googleClient;

	@RequestMapping("/yandex")
	public String yandexToken(@RequestParam(name = "code", required = false) String code,
			@RequestParam(name = "state", required = false) String state, HttpSession session, Model model) {
		if (code == null) {
			final String csrfToken = setCSRFInSession(CSRF_YANDEX, session);
			return MessageFormat.format("redirect:{0}{1}&state={2}", yandexClient.getAuthUrl(), yandexClient.getId(), csrfToken);
		}
		yandexTokenService.persistToken(code, session, state, CSRF_YANDEX);
		return "redirect:/";
	}

	@RequestMapping("/dropbox")
	public String dropboxToken(@RequestParam(name = "code", required = false) String code,
			@RequestParam(name = "state", required = false) String state, HttpSession session, Model model) {
		if (code == null) {
			final String csrfToken = setCSRFInSession(CSRF_DROPBOX, session);
			return MessageFormat.format("redirect:{0}{1}&redirect_uri={2}&state={3}", dropboxClient.getAuthUrl(), dropboxClient.getId(),
					dropboxClient.getRedirectUri(), csrfToken);
		}
		dropboxTokenService.persistToken(code, session, state, CSRF_DROPBOX);
		return "redirect:/";
	}

	@RequestMapping("/google")
	public String googleToken(@RequestParam(name = "code", required = false) String code,
			@RequestParam(name = "state", required = false) String state, HttpSession session, Model model) {
		if (code == null) {
			final String csrfToken = setCSRFInSession(CSRF_GOOGLE, session);
			return MessageFormat.format("redirect:{0}{1}&redirect_uri={2}&scope={3}&state={4}", googleClient.getAuthUrl(),
					googleClient.getId(), googleClient.getRedirectUri(), googleClient.getScope(), csrfToken);
		}
		googleTokenService.persistToken(code, session, state, CSRF_GOOGLE);
		return "redirect:/";
	}

	private String setCSRFInSession(String attributeName, HttpSession session) {
		final String csrfToken = UUID.randomUUID().toString();
		session.setAttribute(attributeName, csrfToken);
		return csrfToken;
	}
}
