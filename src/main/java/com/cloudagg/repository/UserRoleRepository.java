package com.cloudagg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloudagg.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
