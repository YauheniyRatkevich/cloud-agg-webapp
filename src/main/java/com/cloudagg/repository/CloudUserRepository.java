package com.cloudagg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloudagg.entity.CloudUser;

public interface CloudUserRepository extends JpaRepository<CloudUser, Long> {

	CloudUser findByLogin(String login);

}
