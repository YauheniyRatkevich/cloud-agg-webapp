package com.cloudagg.entity;

import javax.persistence.Entity;

@Entity
public class YandexAccessToken extends AccessToken {

	private Long expiresIn;

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}
}
