'use strict';
define([
    "jquery",
    "app/service/token.factory"
], function ($, tokenFactory) {

    const TIMEOUT = 1000;

    function makeToken() {
        return "Bearer " + tokenFactory.getToken("dropbox");
    }

    function saveUrlCheckStatusJob(callback, response) {
        var settings = {
            dataType: "json",
            contentType: "application/json",
            url: "https://api.dropboxapi.com/2/files/save_url/check_job_status",
            method: "POST",
            data: JSON.stringify({
                async_job_id: response.async_job_id
            }),
            headers: {
                authorization: makeToken()
            }
        };

        $.ajax(settings).done(function (responseStatus) {
            var status = responseStatus['.tag'];
            if (status === "failed") {
                //TODO handler
                alert("Dropbox save url failed.");
            } else if (status === "complete") {
                if (callback) {
                    callback(response);
                }
            } else {
                setTimeout(function () {
                    saveUrlCheckStatusJob(callback, response);
                }, TIMEOUT);
            }
        });
    }

    return {
        getFiles: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://api.dropboxapi.com/1/metadata/auto" + pathURI,
                method: "GET",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("DROPBOX_GET_FILES_FAIL");
                //TODO handle failed requests
            });
        },
        deleteFile: function (path, callback) {
            var settings = {
                dataType: "json",
                contentType: "application/json",
                url: "https://api.dropboxapi.com/2/files/delete",
                method: "POST",
                data: JSON.stringify({
                    path: path
                }),
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("DROPBOX_DELETE_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        downloadFile: function (path, callback) {
            var settings = {
                dataType: "json",
                url: "https://api.dropboxapi.com/1/media/auto" + encodeURIComponent(path),
                method: "POST",
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                //TODO ADD FOLDER DOWNLOADING (workaround with share links)
                if (callback) {
                    callback(response);
                }
            }).fail(function (response) {
                if (response.responseJSON.error === "Creating a link for a directory is not allowed.") {
                    alert("Dropbox folder download has not implemented yet");
                }
                console.log("DROPBOX_DOWNLOAD_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        previewFile: function (path, callback) {
            if (callback) {
                if (path.split('.').pop() === "pdf") {
                    this.downloadFile(path, callback);
                } else {
                    callback("https://content.dropboxapi.com/1/previews/auto/" + encodeURIComponent(path)
                        + "?access_token=" + tokenFactory.getToken("dropbox"));
                }
            }
        },
        copyFile: function (filePathSrc, dirDest, callback, overwrite) {
            var thisModule = this;
            var pathFrom = filePathSrc;
            var pathTo = (dirDest === "/" ? "" : dirDest) + "/" + (overwrite ? "Copy of " : "") + filePathSrc.split('/').pop();

            if (pathFrom === pathTo) {
                return;
            }

            var settings = {
                dataType: "json",
                contentType: "application/json",
                url: "https://api.dropboxapi.com/2/files/copy",
                method: "POST",
                data: JSON.stringify({
                    from_path: pathFrom,
                    to_path: pathTo
                }),
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function (response) {
                if (!overwrite && response.status === 409) {
                    thisModule.copyFile(filePathSrc, dirDest, callback, true);
                } else {
                    console.log("DROPBOX_COPY_FILE_FAIL");
                    //TODO handle failed requests
                }
            });
        },
        saveUrl: function (fileName, dirDest, url, callback) {
            var filePath = (dirDest === "/" ? "" : dirDest) + "/" + fileName;

            var settings = {
                dataType: "json",
                contentType: "application/json",
                url: "https://api.dropboxapi.com/2/files/save_url",
                method: "POST",
                data: JSON.stringify({
                    path: filePath,
                    url: url
                }),
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                saveUrlCheckStatusJob(callback, response);
            }).fail(function (response) {
                console.log("DROPBOX_SAVE_URL_FILE_FAIL");
            });
        },
        renameFile: function (filePathSrc, fileNewName, callback) {
            var pathFrom = filePathSrc;
            var pathTo = filePathSrc.substr(0, filePathSrc.lastIndexOf('/')) + '/' + fileNewName;

            if (pathFrom === pathTo) {
                if (callback) {
                    callback();
                }
                return;
            }

            var settings = {
                dataType: "json",
                contentType: "application/json",
                url: "https://api.dropboxapi.com/2/files/move",
                method: "POST",
                data: JSON.stringify({
                    from_path: pathFrom,
                    to_path: pathTo
                }),
                headers: {
                    authorization: makeToken()
                }
            };

            $.ajax(settings).done(function (response) {
                //TODO ADD FOLDER DOWNLOADING (workaround with share links)
                if (callback) {
                    callback(response);
                }
            }).fail(function (response) {
                //TODO handle failed requests
                console.log("DROPBOX_RENAME_FILE_FAIL");
            });
        }
    };
});