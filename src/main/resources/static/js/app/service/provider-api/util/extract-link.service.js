'use strict';
/**
 * extracts link from a response.
 */
define([], function () {
    return {
        download: {
            yandex: function (response) {
                return response.href;
            },
            dropbox: function (response) {
                return response.url;
            },
            google: function (response) {
                if (response.downloadUrl) {
                    return response.downloadUrl + "&access_token=" + response.authorization;
                } else if (response.exportLinks) {
                    for (var exportLink in response.exportLinks) {
                        if (_.startsWith(exportLink, "application/vnd.openxmlformats-officedocument")) {
                            return response.exportLinks[exportLink] + "&access_token=" + response.authorization;

                        }
                    }
                }
            }

        },
        preview: {
            yandex: function (response) {
                return response;
            },
            dropbox: function (response) {
                return response.url || response;
            },
            google: function (response) {
                return response.webViewLink;
            }

        }
    }
});