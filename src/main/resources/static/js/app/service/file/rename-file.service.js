'use strict';
define([
    "jquery",
    "app/service/provider-api/yandex-api",
    "app/service/provider-api/dropbox-api",
    "app/service/provider-api/google-api",
    "app/service/routes"
], function ($, yandexAPI, dropboxAPI, googleAPI, routes) {
    var fileNameSrc;
    var filePathSrc;
    var providerName;

    var providersAggregator = {
        yandex: yandexAPI,
        dropbox: dropboxAPI,
        google: googleAPI
    };

    return {
        openDialog: function (fileName, filePath, provider) {
            fileNameSrc = fileName;
            filePathSrc = filePath;

            providerName = provider;
            var $renameDialog = $('#renameDialog');
            var $renameNewName = $('#renameNewName');
            $renameNewName.val('');
            $renameDialog.modal();
            $renameDialog.on('shown.bs.modal', function () {
                $renameNewName.val(fileNameSrc);
                var lastIndexOfDot = fileName.lastIndexOf('.');
                $renameNewName[0].setSelectionRange(0, lastIndexOfDot !== -1 ? lastIndexOfDot : fileName.length);
                $renameNewName[0].focus();
            });
        },
        closeDialog: function (scope) {
            if (!window.location.hash) {
                //TODO alert
                alert("Wrong URL!");
                return;
            }

            var fileNewName = $('#renameNewName').val();
            $('#renameDialog').modal('hide');

            scope.showLoader[providerName] = true;
            scope.$scan();

            providersAggregator[providerName].renameFile(filePathSrc, fileNewName, function () {
                routes.refreshContent(providerName);
            });

        }
    }
});