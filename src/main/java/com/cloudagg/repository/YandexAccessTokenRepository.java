package com.cloudagg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloudagg.entity.YandexAccessToken;

public interface YandexAccessTokenRepository extends JpaRepository<YandexAccessToken, Long> {

}
