package com.cloudagg.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "cloud_users")
public class CloudUser {

	@Id
	@GeneratedValue
	private Long userId;

	@Column(unique = true)
	private String login;

	@Column(unique = true)
	private String email;

	@Column(length = 72)
	private String password;

	private LocalDateTime registrationDate;

	private boolean enabled;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<UserRole> userRoles;

	@OneToOne
	@PrimaryKeyJoinColumn
	private YandexAccessToken yandexAccessToken;

	@OneToOne
	@PrimaryKeyJoinColumn
	private YandexUser yandexUser;

	@PrePersist
	public void prePersist() {
		registrationDate = LocalDateTime.now();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public YandexAccessToken getYandexAccessToken() {
		return yandexAccessToken;
	}

	public void setYandexAccessToken(YandexAccessToken yandexAccessToken) {
		this.yandexAccessToken = yandexAccessToken;
	}

	public YandexUser getYandexUser() {
		return yandexUser;
	}

	public void setYandexUser(YandexUser yandexUser) {
		this.yandexUser = yandexUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CloudUser other = (CloudUser) obj;
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

}
