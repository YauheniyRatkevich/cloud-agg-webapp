package com.cloudagg.service.oauth;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.cloudagg.configuration.bean.OAuth2ClientConfiguration;
import com.cloudagg.converter.EntityConverter;
import com.cloudagg.dto.SimpleTokenDTO;
import com.cloudagg.entity.AccessToken;
import com.cloudagg.entity.CloudUser;
import com.cloudagg.entity.DropboxAccessToken;
import com.cloudagg.entity.GoogleAccessToken;
import com.cloudagg.entity.YandexAccessToken;
import com.cloudagg.repository.CloudUserRepository;
import com.cloudagg.service.exception.StatesAreNotEqualException;

public abstract class OAuth2TokenService<AccessTokenDTO extends SimpleTokenDTO> {

	private static final String YANDEX_ACCESS_TOKEN = "yandexAccessToken";

	private static final String DROPBOX_ACCESS_TOKEN = "dropboxAccessToken";

	private static final String GOOGLE_ACCESS_TOKEN = "googleAccessToken";

	@Autowired
	private CloudUserRepository cloudUserRepository;

	@SuppressWarnings("unchecked")
	@Secured({ "ROLE_USER", "ROLE_ADMIN" })
	public void persistToken(final String code, final HttpSession session, final String state, final String csrfTokenAttributeName) {
		final String sessionCSRFToken = (String) session.getAttribute(csrfTokenAttributeName);
		if (sessionCSRFToken != null && state != null) {
			if (sessionCSRFToken.equals(state)) {
				session.removeAttribute(csrfTokenAttributeName);
			} else {
				//TODO msg
				throw new StatesAreNotEqualException();
			}
		} else {
			//TODO msg
			throw new StatesAreNotEqualException();
		}

		final AccessTokenDTO accessTokenDTO = requestToken(code);
		final AccessToken accessToken = ((EntityConverter<SimpleTokenDTO, AccessToken>) getEntityConverter()).dtoToEntity(accessTokenDTO);
		final CloudUser cloudUser = cloudUserRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		accessToken.setUserId(cloudUser.getUserId());
		setAccessTokenToSession(accessToken, session);
		getTokenRepository().save(accessToken);
	}

	private AccessTokenDTO requestToken(final String code) {
		final RestTemplate restTemplate = new RestTemplate();
		restTemplate.setMessageConverters(Arrays.asList(new FormHttpMessageConverter(), new MappingJackson2HttpMessageConverter()));
		final MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
		//TODO extract to properties
		form.add("grant_type", "authorization_code");
		form.add("code", code);
		form.add("client_id", getClientConfiguration().getId());
		form.add("client_secret", getClientConfiguration().getSecret());
		adjustRequestTokenForm(form);
		// TODO change error handling
		final ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler() {

			@Override
			public void handleError(ClientHttpResponse response) throws IOException {
				final String errorBody = IOUtils.toString(response.getBody(), "UTF-8");
				//TODO logger
				System.err.println(errorBody);

				final HttpStatus statusCode = response.getStatusCode();
				switch (statusCode.series()) {
					case CLIENT_ERROR: {
						//TODO
						break;
					}
					case SERVER_ERROR: {
						//TODO
						break;
					}
					default:
						throw new RestClientException("Unknown status code [" + statusCode + "]");
				}
			}
		};
		restTemplate.setErrorHandler(errorHandler);
		AccessTokenDTO accessTokenDTO = null;
		try {
			@SuppressWarnings("unchecked")
			final Class<AccessTokenDTO> genericDTOType = (Class<AccessTokenDTO>) GenericTypeResolver.resolveTypeArguments(getClass(),
					OAuth2TokenService.class)[0];
			accessTokenDTO = restTemplate.postForObject(getClientConfiguration().getTokenUrl(), form, genericDTOType);
		} catch (final RestClientException exc) {
			//FIXME wrapper exc
			exc.printStackTrace();
		}
		return accessTokenDTO;
	}

	public static void setAccessTokenToSession(AccessToken accessToken, HttpSession session) {
		if (accessToken != null) {
			String name;
			if (accessToken instanceof YandexAccessToken) {
				name = YANDEX_ACCESS_TOKEN;
			} else if (accessToken instanceof DropboxAccessToken) {
				name = DROPBOX_ACCESS_TOKEN;
			} else if (accessToken instanceof GoogleAccessToken) {
				name = GOOGLE_ACCESS_TOKEN;
			} else {
				//TODO: make own exc
				throw new UnsupportedOperationException("No token of this type found");
			}
			session.setAttribute(name, accessToken);
		}
	}

	@SuppressWarnings("rawtypes")
	public abstract EntityConverter getEntityConverter();

	@SuppressWarnings("rawtypes")
	public abstract CrudRepository getTokenRepository();

	public abstract OAuth2ClientConfiguration getClientConfiguration();

	protected void adjustRequestTokenForm(final MultiValueMap<String, String> form) {
	}

}