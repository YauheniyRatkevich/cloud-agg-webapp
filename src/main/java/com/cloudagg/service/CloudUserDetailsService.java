package com.cloudagg.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.cloudagg.entity.CloudUser;
import com.cloudagg.repository.CloudUserRepository;

@Component
public class CloudUserDetailsService implements UserDetailsService {

	@Autowired
	private CloudUserRepository cloudUserRepository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Assert.notNull(username, "username can't be null");
		CloudUser user = cloudUserRepository.findByLogin(username);
		//TODO check if user not null
		List<SimpleGrantedAuthority> authorities = user.getUserRoles().stream()
				.map(userRole -> new SimpleGrantedAuthority(userRole.getRole()))
				.collect(Collectors.toList());
		return new User(user.getLogin(), user.getPassword(),
				user.isEnabled(), true, true, true, authorities);

	}

}
