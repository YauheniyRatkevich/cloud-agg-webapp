package com.cloudagg.service.exception;


public class PasswordTooShortException extends RuntimeException {

	private static final long serialVersionUID = -58351457063930070L;

	public PasswordTooShortException() {
	}

	public PasswordTooShortException(String message) {
		super(message);
	}

	public PasswordTooShortException(Throwable cause) {
		super(cause);
	}

	public PasswordTooShortException(String message, Throwable cause) {
		super(message, cause);
	}

	public PasswordTooShortException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
