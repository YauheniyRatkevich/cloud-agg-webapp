package com.cloudagg.converter;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.cloudagg.dto.DropboxTokenDTO;
import com.cloudagg.entity.DropboxAccessToken;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DropboxTokenConverter extends EntityConverter<DropboxTokenDTO, DropboxAccessToken> {

}
