'use strict';
define([
    "alight",
    "jquery",
    "lodash",
    "app/service/token.factory",
    "app/service/routes",
    "app/service/file/copy-move-file.service",
    "app/service/file/rename-file.service",

    "split"
], function (alight, $, _, tokenFactory, routes, copyMoveFileService, renameFileService) {
    alight.controllers.providerController = function (scope) {
        var hasYandexToken = !!tokenFactory.getToken("yandex");
        var hasDropboxToken = !!tokenFactory.getToken("dropbox");
        var hasGoogleToken = !!tokenFactory.getToken("google");

        scope.noneShown = !hasYandexToken && !hasDropboxToken && !hasGoogleToken;
        scope.providers = [{
            name: "yandex",
            oauth: hasYandexToken ? "" : "/oauth/yandex",
            shown: hasYandexToken,
            activated: hasYandexToken
        }, {
            name: "dropbox",
            oauth: hasDropboxToken ? "" : "/oauth/dropbox",
            shown: hasDropboxToken,
            activated: hasDropboxToken
        }, {
            name: "google",
            oauth: hasGoogleToken ? "" : "/oauth/google",
            shown: hasGoogleToken,
            activated: hasGoogleToken
        }];

        scope.isCopyDialog = false;

        scope.showView = function (provider) {
            provider.activated = true;
            if (provider.oauth) {
                window.location.href = provider.oauth;
            } else {
                provider.shown = !provider.shown;

                var shownProviders = _.filter(scope.providers, {
                    shown: true
                });
                scope.noneShown = !shownProviders.length;
            }
        };

        scope.chooseCopyDir = function (provider) {
            _.forEach(scope.providers, function (eachProvider) {
                eachProvider.shown = false;
            });
            provider.shown = true;
            scope.isCopyDialog = true;

            copyMoveFileService.closeDialog(scope, provider.name);
        };

        scope.copyFileBtnSubmit = function (isSubmit) {
            scope.isCopyDialog = false;
            if (isSubmit) {
                copyMoveFileService.copyFileSubmit(scope);
            }
        };

        scope.renameFileSubmit = function ($event) {
            if (!$event || $event.keyCode === 13) {
                renameFileService.closeDialog(scope);
            }
        };


        function splitView() {
            $(".gutter").remove();

            var shownProviders = _.filter(scope.providers, {
                shown: true
            });

            if (shownProviders.length) {
                Split(_.map(shownProviders, function (obj) {
                    return "#" + obj.name;
                }), {
                    direction: 'vertical',
                    snapOffset: 0,
                    minSize: 150
                });
            }
        }

        function refresh() {
            splitView();
        }

        // scope.$watchGroup(['$finishBinding', '$any'], splitView);
        scope.$watch('$finishBinding', refresh);
        scope.$watch('$any', refresh);

        scope.files = {
            yandex: [],
            dropbox: [],
            google: []
        };

        scope.providerInfo = {
            yandex: {},
            dropbox: {},
            google: {}
        };

        scope.showLoader = {
            yandex: false,
            dropbox: false,
            google: false
        };

        routes.registerRoutes(scope);
    }
});