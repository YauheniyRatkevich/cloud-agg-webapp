package com.cloudagg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloudagg.entity.GoogleAccessToken;

public interface GoogleAccessTokenRepository extends JpaRepository<GoogleAccessToken, Long> {

}
