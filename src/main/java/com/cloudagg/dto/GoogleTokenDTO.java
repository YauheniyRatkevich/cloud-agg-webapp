package com.cloudagg.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleTokenDTO extends SimpleTokenDTO {

	private static final long serialVersionUID = -4465519768511167816L;

	@JsonProperty("expires_in")
	private Long expiresIn;

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}
}
