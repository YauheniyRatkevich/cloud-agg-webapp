package com.cloudagg.service.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.cloudagg.configuration.bean.DropboxOAuth2ClientConfiguration;
import com.cloudagg.converter.DropboxTokenConverter;
import com.cloudagg.dto.DropboxTokenDTO;
import com.cloudagg.repository.DropboxAccessTokenRepository;

@Service
@ConfigurationProperties(prefix = "oauth.dropbox.client")
public class DropboxOAuth2TokenService extends OAuth2TokenService<DropboxTokenDTO> {

	@Autowired
	private DropboxOAuth2ClientConfiguration clientConfiguration;

	@Autowired
	private DropboxTokenConverter entityConverter;

	@Autowired
	private DropboxAccessTokenRepository tokenRepository;

	@Override
	public DropboxTokenConverter getEntityConverter() {
		return entityConverter;
	}

	@Override
	public DropboxAccessTokenRepository getTokenRepository() {
		return tokenRepository;
	}

	@Override
	protected void adjustRequestTokenForm(MultiValueMap<String, String> form) {
		form.add("redirect_uri", clientConfiguration.getRedirectUri());
	}

	@Override
	public DropboxOAuth2ClientConfiguration getClientConfiguration() {
		return clientConfiguration;
	}

}