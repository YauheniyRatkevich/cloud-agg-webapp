'use strict';
define([
    "alight",
    "lodash"
], function (alight, _) {
    alight.filters.capitalize = function (value) {
        return _.capitalize(value);
    }
});