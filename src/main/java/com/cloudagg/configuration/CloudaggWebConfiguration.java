package com.cloudagg.configuration;

import java.util.stream.Stream;

import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class CloudaggWebConfiguration {

	private static final String H2_CONSOLE_URI = "/console/*";

	private static final String[] CONFIGURATION_FILES = { "oauth-config.yml" };

	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
		YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
		yaml.setResources(Stream.of(CONFIGURATION_FILES).map(ClassPathResource::new).toArray(ClassPathResource[]::new));
		propertySourcesPlaceholderConfigurer.setProperties(yaml.getObject());
		return propertySourcesPlaceholderConfigurer;
	}

	@Bean
	public ServletRegistrationBean h2servletRegistration() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
		registrationBean.addUrlMappings(H2_CONSOLE_URI);
		return registrationBean;
	}

}
