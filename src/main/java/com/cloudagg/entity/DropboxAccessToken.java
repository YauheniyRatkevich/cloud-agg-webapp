package com.cloudagg.entity;

import javax.persistence.Entity;

@Entity
public class DropboxAccessToken extends AccessToken {

	private String uid;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
