'use strict';
define([
    "jquery",
    "app/service/token.factory"
], function ($, tokenFactory) {

    const TIMEOUT = 1000;

    function checkOperationStatus(callback, response) {
        var settings = {
            dataType: "json",
            url: response.href,
            method: "GET",
            headers: {
                authorization: tokenFactory.getToken("yandex")
            }
        };
        $.ajax(settings).done(function (responseStatus) {
            var status = responseStatus.status;
            if (status === "failure") {
                //TODO handler
                alert("YandexDisk Operation failed.");
            } else if (status === "success") {
                if (callback) {
                    callback(response);
                }
            } else {
                setTimeout(function () {
                    checkOperationStatus(callback, response);
                }, TIMEOUT);
            }
        });
    }

    return {
        getFiles: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var previewSize = "S";
            var settings = {
                dataType: "json",
                url: "https://cloud-api.yandex.net:443/v1/disk/resources?path=" + pathURI
                + "&preview_crop=true&fields=_embedded.items&preview_size=" + previewSize,
                method: "GET",
                headers: {
                    authorization: tokenFactory.getToken("yandex")
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("YANDEX_GET_FILES_FAIL");
                //TODO handle failed requests
            });
        },
        deleteFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://cloud-api.yandex.net:443/v1/disk/resources?path=" + pathURI,
                method: "DELETE",
                headers: {
                    authorization: tokenFactory.getToken("yandex")
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("YANDEX_DELETE_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        downloadFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);
            var settings = {
                dataType: "json",
                url: "https://cloud-api.yandex.net/v1/disk/resources/download?path=" + pathURI,
                method: "GET",
                headers: {
                    authorization: tokenFactory.getToken("yandex")
                }
            };

            $.ajax(settings).done(function (response) {
                if (callback) {
                    callback(response);
                }
            }).fail(function () {
                console.log("YANDEX_DOWNLOAD_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        previewFile: function (path, callback) {
            var pathURI = encodeURIComponent(path);

            function getFileMetaInfo() {
                return $.ajax({
                    dataType: "json",
                    url: "https://cloud-api.yandex.net:443/v1/disk/resources?fields=public_key%2Cname&path=" + pathURI,
                    method: "GET",
                    headers: {
                        authorization: tokenFactory.getToken("yandex")
                    }
                });
            }

            function publishFile() {
                return $.ajax({
                    dataType: "json",
                    url: "https://cloud-api.yandex.net:443/v1/disk/resources/publish?path=" + pathURI,
                    method: "PUT",
                    headers: {
                        authorization: tokenFactory.getToken("yandex")
                    }
                });
            }

            function unpublishURL() {
                return $.ajax({
                    dataType: "json",
                    url: "https://cloud-api.yandex.net:443/v1/disk/resources/unpublish?path=" + pathURI,
                    method: "PUT",
                    headers: {
                        authorization: tokenFactory.getToken("yandex")
                    }
                });
            }

            function generatePreviewURL(response) {
                return "https://docviewer.yandex.ru/?url=ya-disk-public://" + encodeURIComponent(response.public_key)
                    + "&name=" + encodeURIComponent(response.name);
            }

            getFileMetaInfo().done(function (response) {
                if (callback) {
                    if (response.public_key) {
                        callback(generatePreviewURL(response));
                    }
                    else {
                        publishFile().then(getFileMetaInfo).then(function (response) {
                            if (response.public_key) {
                                callback(generatePreviewURL(response));
                            }
                        }).then(unpublishURL).fail(function () {
                            console.log("YANDEX_PREVIEW_FILE_FAIL");
                            //TODO handle failed requests
                        });
                    }
                }
            }).fail(function () {
                console.log("YANDEX_PREVIEW_FILE_FAIL");
                //TODO handle failed requests
            });
        },
        copyFile: function (filePathSrc, dirDest, callback, overwrite) {
            var thisModule = this;
            var pathURIFrom = encodeURIComponent(filePathSrc);
            var pathURITo = encodeURIComponent((dirDest === "/" ? "disk:" : dirDest) + "/" + filePathSrc.split('/').pop());

            if (pathURIFrom === pathURITo) {
                if (callback) {
                    callback();
                }
                return;
            }
            var settings = {
                dataType: "json",
                url: "https://cloud-api.yandex.net/v1/disk/resources/copy?from=" + pathURIFrom + "&path=" + pathURITo + "&overwrite=" + !!overwrite,
                method: "POST",
                headers: {
                    authorization: tokenFactory.getToken("yandex")
                }
            };

            $.ajax(settings).done(function (response, textStatus, xhr) {
                if (xhr.status === 202) {
                    checkOperationStatus(callback, response);
                } else {
                    if (callback) {
                        callback(response);
                    }
                }
            }).fail(function (response) {
                if (response.responseJSON.error === "DiskResourceAlreadyExistsError") {
                    //TODO confim overwrite dialog
                    if (confirm("File already exists. Overwrite file?")) {
                        thisModule.copyFile(filePathSrc, dirDest, callback, true);
                    }
                } else {
                    //TODO handle failed requests
                    console.log("YANDEX_COPY_FILE_FAIL");
                }
            });
        },
        saveUrl: function (fileName, dirDest, url, callback) {
            var thisModule = this;
            var urlEncoded = encodeURIComponent(url);
            var filePathEncoded = encodeURIComponent((dirDest === "/" ? "disk:" : dirDest) + "/" + fileName);

            var settings = {
                dataType: "json",
                url: "https://cloud-api.yandex.net/v1/disk/resources/upload?url=" + urlEncoded + "&path=" + filePathEncoded,
                method: "POST",
                headers: {
                    authorization: tokenFactory.getToken("yandex")
                }
            };

            $.ajax(settings).done(function (response, textStatus, xhr) {
                if (xhr.status === 202) {
                    checkOperationStatus(callback, response);
                } else {
                    if (callback) {
                        callback(response);
                    }
                }
            }).fail(function (response) {
                //TODO handle failed requests
                console.log("YANDEX_SAVE_URL_FAIL");
            });
        },
        renameFile: function (filePathSrc, fileNewName, callback, overwrite) {
            var thisModule = this;
            var pathURIFrom = encodeURIComponent(filePathSrc);
            var pathURITo = encodeURIComponent(filePathSrc.substr(0, filePathSrc.lastIndexOf('/')) + '/' + fileNewName);

            if (pathURIFrom === pathURITo) {
                if (callback) {
                    callback();
                }
                return;
            }
            var settings = {
                dataType: "json",
                url: "https://cloud-api.yandex.net/v1/disk/resources/move?from=" + pathURIFrom + "&path=" + pathURITo + "&overwrite=" + !!overwrite,
                method: "POST",
                headers: {
                    authorization: tokenFactory.getToken("yandex")
                }
            };

            $.ajax(settings).done(function (response, textStatus, xhr) {
                if (xhr.status === 202) {
                    checkOperationStatus(callback, response);
                } else {
                    if (callback) {
                        callback(response);
                    }
                }
            }).fail(function (response) {
                if (response.responseJSON.error === "DiskResourceAlreadyExistsError") {
                    //TODO confim overwrite dialog
                    if (confirm("File already exists. Overwrite file?")) {
                        thisModule.renameFile(filePathSrc, dirDest, callback, true);
                    }
                } else {
                    console.log("YANDEX_RENAME_FILE_FAIL");
                    //TODO handle failed requests
                }
            });
        }
    };
});